<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	/*
		Utilize essa variavel se sua tabela
		tiver um nome 'fora do padrao'
	 */
	// protected $table = 'articles';

	/*
		Essas variaveis tem uso muito especifico.
		Veja mais detalhes na documentacao:
		https://laravel.com/docs/5.5/eloquent
	 */

    protected $fillable = [
    	'title',
    	'content',
    	'image_path'
    ];

    protected $hidden = [
    	'id',
    	'created_at',
    	'updated_at'
    ];
    
}
