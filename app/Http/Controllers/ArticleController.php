<?php

namespace App\Http\Controllers;

use App\Article;

use Illuminate\Http\Request;

/*
    Para saber o que cada função deve fazer,
    Veja o arquivo StandardController
 */
class ArticleController extends Controller
{
    public function index()
	{
        // Pega todos os elementos em ordem decrescente
		$articles = Article::orderBy('id', 'desc')->get();

		return view('home.index')->with(['articles' => $articles]);
	}

    public function create()
    {
        return view('article.create');
    }

    public function store(Request $request)
    {
        // Recebe o arquivo e guarda-o em $image
        $image = $request->file('image');

        if($image) // Se realmente tem um arquivo,
        {
            /*
                Guarda na pasta storage/app/public/ 
                e apaga o pedaço 'public/'
             */
            $image = $image->store('public');
            $image = str_replace('public/', '', $image);
        }

        // Cria o recurso (via objeto)
        /*
           $article = new Article;
           $article->title = $request['title'];
           $article->content = $request['content'];
           $article->image_path = $image;
           $article->save();
         */


        // Cria o recurso (via método estático)
        Article::create([
            'title' => $request['title'],
            'content' => $request['content'],
            'image_path' => $image
        ]);

        return redirect()->route('home');
    }

    public function show($id)
    {
        // Encontra o elemento com o $id recebido
        $article = Article::find($id);

        return view('article.show')->with(['article' => $article]);
    }

    public function edit($id)
    {
        // Encontra o elemento com o $id recebido
        $article = Article::find($id);

        return view('article.edit')->with(['article' => $article]);
    }

    public function update($id, Request $request)
    {
        // Encontra o elemento com o $id recebido
        $article = Article::find($id);

        // Recebe o arquivo e guarda-o em $image
        $image = $request->file('image');

        if($image) // Se realmente tem um arquivo,
        {
            /*
                Guarda na pasta storage/app/public/ 
                e apaga o pedaço 'public/'
             */
            $image = $image->store('public');
            $image = str_replace('public/', '', $image);
            $article->image_path = $image;
        }

        // Atualiza o recurso (via objeto)
        $article->title = $request['title'];
        $article->content = $request['content'];
        $article->save();

        // Atualiza o recurso (via método estático)
        /*
            Article::where('id', '=', $id)->update([
                'title' => $request['title'],
                'content' => $request['content'],
                'image_path' => $image
            ]);
         */

        return redirect()->route('home');
    }

    public function destroy($id)
    {
        // Encontra o elemento com o $id recebido
        $article = Article::find($id);

        /*
            Apaga o arquivo da public/storage/
            A funcao unlink() eh nativa do PHP, busque documentacao.

            A funcao public_path($path) retorna o caminho absoluto
            para a sua pasta public/ e concatena esse caminho com
            o que estiver na variavel $path; em termos praticos,
            sera algo como '/path/to/application/public/' . $path
         */
        unlink(public_path('storage/' . $article->image_path));

        // Apaga a entrada do banco de dados
        $article->delete();

        return redirect()->route('home');
    }
}
