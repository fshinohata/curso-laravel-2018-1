@extends('layout')

@section('content')
<div class="container">
	<form method="POST" action="{{ route('article.update', ['id' => $article->id]) }}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
			<label for="image">Image</label>
			<input type="file" name="image" class="form-control">
		</div>

		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" value="{{ $article->title }}">
		</div>
			
		<div class="form-group">
			<label for="content">Text</label>
			<textarea name="content" rows="10" class="form-control">{{ $article->content }}</textarea>
		</div>

		<button type="submit" class="btn btn-primary">Save</button>
		<a class="btn btn-danger" href="{{ redirect()->back() }}">Cancel</a>
	</form>
</div>
@endsection