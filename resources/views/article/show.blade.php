@extends('layout')

@php ($title = $article->title)
@php ($subtitle = '')

@section('content')
<div id="fh5co-content">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-12">
						<p class="animate-box">{{ $article->content }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="pull-right">
		<form method="POST" action="{{ route('article.destroy', ['id' => $article->id]) }}">
			{{ csrf_field() }}
			<a class="btn btn-primary" href="{{ route('article.edit', ['id' => $article->id]) }}">Edit</a>
			<button type="submit" class="btn btn-primary">Delete</button>
		</form>
	</div>
</div>
@endsection