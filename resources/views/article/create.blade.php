@extends('layout')

@section('content')
<div class="container">
	<form method="POST" action="{{ route('article.store') }}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
			<label for="image">Image</label>
			<input type="file" name="image" class="form-control">
		</div>

		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control">
		</div>
			
		<div class="form-group">
			<label for="content">Text</label>
			<textarea name="content" rows="10" class="form-control"></textarea>
		</div>

		<button type="submit" class="btn btn-primary">Publish</button>
	</form>
</div>
@endsection