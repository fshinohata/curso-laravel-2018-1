<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
	Se nao quiser criar todas essas rotas para todos
	os models, pesquise pela funcao Route::resource() e
	pelo comando 'php artisan route:list'

	Note que o nomeamento das rotas deve ser feito de outra forma:
	https://stackoverflow.com/questions/25290229/laravel-named-route-for-resource-controller

	A funcao where() permite atribuir controle de variaveis nas rotas:
	Como os ids devem ser compostos apenas de numeros, basta usar um Regex que
	englobe 'apenas numeros'
 */

Route::get('/', 'ArticleController@index')->name('home');
Route::get('/article/new', 'ArticleController@create')->name('article.create');
Route::post('/article/new', 'ArticleController@store')->name('article.store');
Route::get('/article/read/{id}', 'ArticleController@show')->name('article.show')->where(['id' => '[0-9]+']);
Route::get('/article/edit/{id}', 'ArticleController@edit')->name('article.edit')->where(['id' => '[0-9]+']);
Route::post('/article/edit/{id}', 'ArticleController@update')->name('article.update')->where(['id' => '[0-9]+']);
Route::post('/article/destroy/{id}', 'ArticleController@destroy')->name('article.destroy')->where(['id' => '[0-9]+']);
